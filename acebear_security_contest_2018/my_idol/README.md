# AceBear Security Contest: My idol

[FR](#fr-version)

## Challenge details

| Event                    | Challenge | Category      | Points | Solves |
|--------------------------|-----------|---------------|--------|--------|
| AceBear Security Contest | My idol   | Misc/Forensic | 971    | 6      |

Download: [call_him.ape](challenge/call_him.ape), [Google Drive](https://drive.google.com/file/d/1m8qj06PBx1jt3VWhOWGGTHr1OAXVFdPh/view)

Author: [komang4130](https://komang4130.wordpress.com/)

### Description

> Trying to listen and maybe you know his name, and you'll know one of his secret weapons.
>
> Something that he deleted from Michael Hansen may help you get in.

## TL;DR

If you are not interested in the detailed answer to the question "How to solve this problem?",
the short answer is that using attributes of our daily life and/or the public domain
to secure access to our data is not a good idea.

Having quickly identified the author's interest in the series Mr. Robot, digging a little deeper,
we were able to (more or less quickly) identify processes and techniques
used by the author to conceal his data.

This is just a reflection within reach of those who would have taken the time to watch
at least the first episode of the series.

Morality: Watch [Mr. Robot](http://www.usanetwork.com/mrrobot)!

## Methology

Reading the [description of the challenge](#description), we are advised that by listening to the soundtrack,
we would be likely to find a person's name and one of its secret weapons.

Listening to the soundtack, we can recognize quite easily a trailer of the season 1 of Mr. Robot
published on [YouTube](https://www.youtube.com/watch?v=xIBiJ_SzJTA).

Having a few hours of watching and analysis of the Mr. Robot series, this first part seems to be trivial.
Elliot uses different ways to hide data, but let's start with the simplest way to avoid wasting time.

As this is an audio stream, let's use our must-have audio processing tool: [Audacity](http://www.audacityteam.org/).

The format is not recognized by Audacity, let's try to import it in RAW and display the spectrogram:

![illustrations/spectrogram.png](illustrations/spectrogram.png)

Nothing... Hence, this is not [spectrogram-based steganography](https://solusipse.net/blog/post/basic-methods-of-audio-steganography-spectrograms/).

Another technique used by Elliot throughout the series is to conceal data in
audio files that he then burns to CD and hides in his room. To do this, it uses a
free and fairly simple to use software: [DeepSound](http://www.jpinsoft.net/DeepSound/).

Let's try to open the file in DeepSound:

![illustrations/deepsound_test.png](illustrations/deepsound_test.png)

Yeah! We must now find the password used by the author to hide the data...

Here is a non-exhaustive list of attempts (with case variations):

 - `ER28-0652`: Elliot Alderson's employee id at AllSafe Security
 - `Mr. Robot`: the series' name
 - `Rami Malek`: actor playing Elliot Alderson
 - `Sam Esmail`: series director
 - `Flipper`: name of Lenny Shannon's dog that Elliot stole.
 - `Michael Hansen`: pseudonym used by Krista's ex-boyfriend
 - `Lenny Shannon`: real name of Michael Hansen
 - `Ashley Madison`: a (real) data leak that was used to gather information about Lenny Shannon
 - `Taxi`: used to track down Lenny Shannon
 - `Pierre Loti`: restaurant where Krista and Michael Hansen were having dinner
 - `Krista Gordon`: Elliot's psychotherapist

No variation seems to work... Let's keep thinking!

The file is named `call_me`!

While replaying the episodes in my head (yes, I'm a fan of the series!), I remember very precisely
the first time Elliot Alderson met Michael Hansen in the first episode:

Michael Hansen walks his dog Flipper around and is being aggressive. Elliot attends the scene and asks Michael Hansen
if he can use his phone to call his mother. Operating the social engineering leverage process,
Michael Hansen is forced to lend him his phone. After dialling his own number,
Elliot finally deletes its traces from the call log, allowing us to view its phone number
along with some of Michael Hansen's recent calls.

![illustrations/mr_robot_call_log.png](illustrations/mr_robot_call_log.png)

We try again with Elliot's phone number: `(212) 555-0179`, it doesn't work...

After trying with the contacts names from the call log, I finally get back to the Elliot's phone number, but 
try different variations:

 - `(212) 555-0179`
 - `(212)555-0179`
 - `212 555-0179`
 - `212-555-0179`
 - `555-0179`
 - `5550179`
 - `2125550179`

Yes!! The password is `2125550179`!

A [`weird`](files/weird) file is hidden in the sound file:

```raw
01359205248365658530218751046900529993719451836050699KKFmwth7962005420727881418598892923382GIfzpMEJDxXlkKzQwxLvUV3541654892946776010272388584017314623691362754qEeYfOJ348091412907133545573039596290640125315408140410815621
...
```

At first glance, this file looks like a file from the encoding of binary data into base64
using [base64(1)](https://linux.die.net/man/1/base64) tool.

Let's try to decode the data:

```bash
# base64 -d weird >base64_decoded
# file base64_decoded 
base64_decoded: data
# strings base64_decoded 
yd+40
N+(eb
j`JWz
TPdN
.U%,qXG
...
```

Nothing interesting when decoding the data, the data, the output binary file doesn't pass any test
(file system analysis, search for magic bytes, language analysis, etc.).

Let's try something else, in the series, the goal is to bring down the E Corp conglomerate!
In order to do this, the FSociety group encrypts all their data, but
keeps the private key of the key pair used to encrypt this data.

In the last episode of season 3, Elliot decides to cancel the operation by sending the encryption key
to E Corp's recovery team in order to recover access to the data.

![illustrations/mr_robot_email_private_key.png](illustrations/mr_robot_email_private_key.png)

Let's try to get the private key!

![illustrations/email_e_corp.png](illustrations/email_e_corp.png)

Once sent, you receive two automatic replies, one containing a ticket number
and the other one containing the [private key](files/priv.key)!

Let's regenerate the public key and try to decrypt the data:

```bash
# openssl asn1parse -in priv.key  # checking the integrity of the private key
# openssl rsa -in priv.key -pubout >pub.key  # public key generation
# openssl rsautl -decrypt -inkey priv.key -out flag < <(base64 -d weird)  # data decryption with private key
RSA operation error
140259665409280:error:0406506C:rsa routines:rsa_ossl_private_decrypt:data greater than mod len:../crypto/rsa/rsa_ossl.c:391:
# openssl rsautl -decrypt -inkey priv.key -out flag < <(head -n1 weird | base64 -d -)
RSA operation error
140490292888832:error:0407109F:rsa routines:RSA_padding_check_PKCS1_type_2:pkcs decoding error:../crypto/rsa/rsa_pk1.c:243:
140490292888832:error:04065072:rsa routines:rsa_ossl_private_decrypt:padding check failed:../crypto/rsa/rsa_ossl.c:477:
```

That doesn't seems to be the right way, too bad!

After few hours discussing with team members,
we end up remembering that QR Code is widely used in the series. What if
the data was not actually encoded in base64, but instead represents a QRCode?

Alphabetical characters `a-zA-Z` being white pixels and numeric characters `0-9` being black pixels? Let's try it out!

```bash
# git clone https://git.bmoine.fr/alpha-to-pixel
# python alpha-to-pixel/alpha_to_pixel.py -i weird
[info] Input file: weird
[info] Output image: output.png
[info] Image size: (220, 220)
[+] output.png created successfully!
```

![illustrations/output.png](illustrations/output.png)

A quick scan with a phone camera and it's done!

![illustrations/qr_code_scan.png](illustrations/qr_code_scan.png)

Final flag:

```
AceBear{25_12_Ng0`i_nh4`_r4_d3`_!!!!k0_c00l}
```

## FR version

### TL;DR

Si vous n'êtes pas intéressé par la réponse détaillée à la question "Comment résoudre ce problème ?",
la réponse courte est que le fait d'utiliser des attributs de notre quotidien et/ou du domaine public
afin de sécuriser l'accès à ses données n'est pas une bonne idée.

Ayant identifié rapidement l'intérêt porté par l'auteur à la série Mr. Robot, en creusant un peu,
nous avons été (plus ou moins rapidement) en capacité d'identifier des processus et techniques
exploitées par l'auteur pour dissimuler ses données.

Il ne s'agit ici que de réflexion à la portée de celles et ceux qui auraient pris le temps de regarder
au moins le premier épisode de la série.

Moralité : regardez [Mr. Robot](http://www.usanetwork.com/mrrobot) !

### Méthodologie

En lisant la [description du challenge](#description), on nous indique qu'en écoutant la bande-son,
nous serions susceptibles de trouver le nom d'une personne et une de ses armes secrètes.

En écoutant la bande-son, on peut reconnaître assez facilement un trailer de la saison 1 de Mr. Robot
diffusé sur [YouTube](https://www.youtube.com/watch?v=xIBiJ_SzJTA).

Ayant quelques heures de visionnage et d'analyse de la série Mr. Robot, cette première partie semble triviale.
Elliot utilise différents moyens pour dissimuler des données, mais commençons par la plus simple afin de
ne pas perdre de temps.

S'agissant d'un flux audio, on ressort notre outil de traitement audio incontournable : [Audacity](http://www.audacityteam.org/).

Le format n'est pas reconnu par Audacity, essayons de l'importer en RAW et d'afficher le spectrogramme :

![illustrations/spectrogram.png](illustrations/spectrogram.png)

Rien... Il ne s'agit donc pas de [stéganographie basée sur le spectrogramme](https://solusipse.net/blog/post/basic-methods-of-audio-steganography-spectrograms/).

Une autre technique utilisée par Elliot tout au long de la série est de dissimuler des données dans
des fichiers audio qu'il grave ensuite sur CD et cache dans sa chambre. Pour cela, il utilise un logiciel
gratuit et plutôt simple à utiliser : [DeepSound](http://www.jpinsoft.net/DeepSound/).

Essayons d'ouvrir le fichier dans DeepSound :

![illustrations/deepsound_test.png](illustrations/deepsound_test.png)

Yeah! Il faut à présent trouver le mot de passe utilisé par l'auteur pour cacher les données...

Voici une liste non exhaustive des tentatives (avec variations sur la casse des caractères) :

 - `ER28-0652` : matricule d'Elliot Alderson chez AllSafe Security
 - `Mr. Robot` : le nom de la série
 - `Rami Malek` : acteur jouant Elliot Alderson
 - `Sam Esmail` : réalisateur de la série
 - `Flipper` : nom de la chienne de Lenny Shannon volée par Elliot
 - `Michael Hansen` : pseudonyme utilisé par l'ex-copain de Krista
 - `Lenny Shannon` : nom réel de Michael Hansen
 - `Ashley Madison` : une fuite de données ayant permis de récupérer des informations sur Lenny Shannon
 - `Taxi` : moyen utilisé pour traquer Lenny Shannon
 - `Pierre Loti` : restaurant dans lequel Krista et Michael Hansen dînaient
 - `Krista Gordon` : psychothérapeute d'Elliot

Aucune variation ne semble fonctionner... Réfléchissons !

Le fichier s'appelle `call_me` !

En me repassant les épisodes dans ma tête (oui, je suis un très grand fan de la série !), je me souviens très
précisément de la première rencontre entre Elliot Alderson et Michael Hansen dans le premier épisode de la série :

Michael Hansen promène sa chienne Flipper et se montre agressif. Elliot assiste à la scène et demande Michael Hansen
s'il peut utiliser son téléphone pour appeler sa mère. Exploitant le procédé de levier de l'ingénierie sociale,
Michael Hansen se retrouve contraint à répondre positivement et lui prêter. Après avoir composé son propre numéro,
Elliot finit par supprimer ses traces de l'historique d'appel, nous permettant au passage de visualiser son numéro
de téléphone ainsi qu'une partie des appels récents de Michael Hansen.

![illustrations/mr_robot_call_log.png](illustrations/mr_robot_call_log.png)

On réessaye avec le numéro de téléphone de Elliot : `(212) 555-0179`, ça ne fonctionne pas...

Après avoir essayé avec les noms des contacts apparaissant dans l'historique d'appel, je finis par reprendre le
numéro de téléphone de Elliot, mais essaie différentes variations :

 - `(212) 555-0179`
 - `(212)555-0179`
 - `212 555-0179`
 - `212-555-0179`
 - `555-0179`
 - `5550179`
 - `2125550179`

Oui !! Le mot de passe est `2125550179` !

Un fichier [`weird`](files/weird) est caché dans le fichier de son :

```raw
01359205248365658530218751046900529993719451836050699KKFmwth7962005420727881418598892923382GIfzpMEJDxXlkKzQwxLvUV3541654892946776010272388584017314623691362754qEeYfOJ348091412907133545573039596290640125315408140410815621
...
```

À première vue, ce fichier ressemble comme deux goûte d'eau à un fichier issu de l'encodage de données binaires en base64
avec l'outil [base64(1)](https://linux.die.net/man/1/base64).

Essayons de décoder les données :

```bash
# base64 -d weird >base64_decoded
# file base64_decoded 
base64_decoded: data
# strings base64_decoded 
yd+40
N+(eb
j`JWz
TPdN
.U%,qXG
...
```

Rien d'intéressant en décodant les données, le fichier binaire obtenu ne passe aucun test
(analyse du système de fichier, recherche de magic bytes, analyse du langage, etc.).

Essayons autre chose, dans la série, l'objectif est de faire tomber le conglomérat E Corp !
Pour cela, le groupe FSociety procède au chiffrement de l'intégralité de leurs données, mais
conserve la clé privée du couple de clés utilisé pour chiffrer ces données.

Dans le dernier épisode de la saison 3, Elliot décide d'annuler l'opération en envoyant la clé
de chiffrement à l'équipe récupération d'E Corp afin de retrouver l'accès aux données.

![illustrations/mr_robot_email_private_key.png](illustrations/mr_robot_email_private_key.png)

Essayons de récupérer la clé privée !

![illustrations/email_e_corp.png](illustrations/email_e_corp.png)

Une fois envoyé, on reçoit deux réponses automatiques, l'une contenant un numéro de ticket
et l'autre la [clé privée](files/priv.key) !

Régénérons la clé publique et essayons de déchiffrer les données :

```bash
# openssl asn1parse -in priv.key  # vérification de l'intégrité de la clé privée
# openssl rsa -in priv.key -pubout >pub.key  # génération de la clé publique
# openssl rsautl -decrypt -inkey priv.key -out flag < <(base64 -d weird)  # déchiffrement des données avec la clé privée
RSA operation error
140259665409280:error:0406506C:rsa routines:rsa_ossl_private_decrypt:data greater than mod len:../crypto/rsa/rsa_ossl.c:391:
# openssl rsautl -decrypt -inkey priv.key -out flag < <(head -n1 weird | base64 -d -)
RSA operation error
140490292888832:error:0407109F:rsa routines:RSA_padding_check_PKCS1_type_2:pkcs decoding error:../crypto/rsa/rsa_pk1.c:243:
140490292888832:error:04065072:rsa routines:rsa_ossl_private_decrypt:padding check failed:../crypto/rsa/rsa_ossl.c:477:
```

Ça ne semble pas être ça, dommage !

Après quelques heures de réflexion et de concertation avec les membres de l'équipe,
on finit par se rappeler que les QR Code sont très présents dans la série. Et si
les données n'étaient en fait pas encodées en base64, mais représentaient un QRCode ?

Les caractères alphabétiques `a-zA-Z` correspondant à des pixels blancs et les caractères
numériques `0-9` à des pixels noirs ? Testons ça !

```bash
# git clone https://git.bmoine.fr/alpha-to-pixel
# python alpha-to-pixel/alpha_to_pixel.py -i weird
[info] Input file: weird
[info] Output image: output.png
[info] Image size: (220, 220)
[+] output.png created successfully!
```

![illustrations/output.png](illustrations/output.png)

Un petit scan avec l'appareil photo de notre téléphone et c'est bon !

![illustrations/qr_code_scan.png](illustrations/qr_code_scan.png)

Flag final :

```
AceBear{25_12_Ng0`i_nh4`_r4_d3`_!!!!k0_c00l}
```
